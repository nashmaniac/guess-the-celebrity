package com.nashmaniac.guessthecelebrity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    ArrayList<String> imageLocations = new ArrayList<String>();
    ArrayList<String> celebrityName = new ArrayList<String>();
    ArrayList<String> arrayList = new ArrayList<String>();
    ImageView imageView;
    Button button0;
    Button button1;
    Button button2;
    Button button3;
    int currentImage = 0;
    int locationOfAnswer = 0;
    public void chooseCelebrity(View view){
        Button b = (Button) view;
        if(b.getTag().equals(Integer.toString(locationOfAnswer))){
            Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getApplicationContext(), "InCorrect. It was "+celebrityName.get(currentImage), Toast.LENGTH_SHORT).show();
        }
        generateQuestion();
    }
    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap>{
        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitmap;
            URL url;
            HttpURLConnection httpURLConnection = null;
            try{
                url = new URL(strings[0]);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
                return bitmap;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    public class DownloadTask extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            URL url;
            HttpURLConnection httpURLConnection = null;

            try{
                url = new URL(strings[0]);
            }catch (Exception e){
                return null;
            }

            try{
                httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                int data = inputStreamReader.read();
                while (data!=-1){
                    result +=((char) data);
                    data = inputStream.read();
                }
                return result;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
    public void generateQuestion(){
        Random rand = new Random();
        currentImage = rand.nextInt(imageLocations.size());

        DownloadImageTask downloadImageTask = new DownloadImageTask();
        try{
            Bitmap bitmap = downloadImageTask.execute(imageLocations.get(currentImage)).get();
            imageView.setImageBitmap(bitmap);
        }catch (Exception e){
            e.printStackTrace();
        }
        arrayList.clear();
        locationOfAnswer = rand.nextInt(4);
        for(int i=0;i<4;i++){
            int incorrectAnswer = rand.nextInt(celebrityName.size());
            if(i==locationOfAnswer){
                arrayList.add(celebrityName.get(currentImage));
            }else{
                while(celebrityName.get(currentImage).equals(celebrityName.get(incorrectAnswer))){
                    incorrectAnswer = rand.nextInt(celebrityName.size());
                }
                arrayList.add(celebrityName.get(incorrectAnswer));
            }
        }

        button0.setText(arrayList.get(0));
        button1.setText(arrayList.get(1));
        button2.setText(arrayList.get(2));
        button3.setText(arrayList.get(3));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imageView = (ImageView) findViewById(R.id.imageView);
        button0 = (Button) findViewById(R.id.button0);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);

        DownloadTask downloadTask = new DownloadTask();
        try{
            String result = downloadTask.execute("http://www.posh24.com/celebrities").get();
            Pattern pattern = Pattern.compile("<img src=\"(.*?)\"");
            Pattern namePattern = Pattern.compile("alt=\"(.*?)\"");
            Matcher matcher = pattern.matcher(result);
            Matcher nameMatcher = namePattern.matcher(result);
            imageLocations.clear();
            while (matcher.find()){
                imageLocations.add(matcher.group(1));
            }
            celebrityName.clear();
            while (nameMatcher.find()){
                celebrityName.add(nameMatcher.group(1));
            }
            generateQuestion();
        }catch (Exception e){
            e.printStackTrace();
        }


//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
